module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'jest': true,
    'node': true
  },
  'extends': [
    'eslint:recommended',
    'plugin:react/recommended'
  ],
  'parser': '@babel/eslint-parser',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 2018,
    'sourceType': 'module',
    'requireConfigFile': false
  },
  'plugins': [
    'react', 'jest'
  ],
  'settings': {
    'react': {
      'version': 'detect'
    }
  },
  'rules': {
    'eqeqeq': 'error',
    'no-trailing-spaces': 'error',
    'no-console': 0,
    'react/prop-types': 0,
    'object-curly-spacing': [
      'error', 'always'
    ],
    'arrow-spacing': [
      'error', { 'before': true, 'after': true }
    ],
    'indent': [
      'error',
      2
    ],
    'linebreak-style': [
      'error',
      'unix'
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
  },
};
