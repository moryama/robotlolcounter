// eslint-disable-next-line
module.exports = {
  // Solve Jest: Support for the experimental syntax 'jsx' isn't currently enabled
  presets: [
    ['@babel/preset-env', { targets: { node: 'current' } }],
    ['@babel/preset-react', { targets: { node: 'current' } }] // add this
  ]
};