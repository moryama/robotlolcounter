# Robot Counter

This robot can count, with a twist. 🤖️🤸‍♀️️

NOTE: This was an assignment for a developer position at a company.
The task was to create a FizzBuzz version using React and Meteor.

## Tools

- MeteorJS
- React
- Jest, React Testing Library
- Bootstrap

## Usage

First install [Meteor](https://www.meteor.com/developers/install).

To run the application:
```
npm install
npm start
```

Find the app running on [localhost://3000](http://localhost:3000/).

To run the tests:
```
npm test
```