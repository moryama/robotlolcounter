const counter = require('./counter');
const helper = require('../../tests/test_helper');


describe('counter function', () => {

  test('returns expected output with known input', () => {
    const countStart = 1;
    const countEnd = 15;
    const expected = helper.count1to15;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns expected output for model case', () => {
    const countStart = 1;
    const countEnd = 100;
    const expected = helper.count1to100;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('string input arguments are converted to numbers', () => {
    const countStart = '-5';
    const countEnd = '-3';
    const expected = helper.countStartAndEndNegatives;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns expected output if arguments are decimal numbers', () => {
    const countStart = 1.2;
    const countEnd = 5.1;
    const expected = helper.countDecimals;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('0 is printed as expected', () => {
    const countStart = 0;
    const countEnd = 0;
    const expected = ['RobotLOL'];

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns expected output if start value is a negative number', () => {
    const countStart = -5;
    const countEnd = 3;
    const expected = helper.countStartNegative;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns expected output if both start and end values are negative numbers', () => {
    const countStart = -5;
    const countEnd = -3;
    const expected = helper.countStartAndEndNegatives;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('can count starting from zero', () => {
    const countStart = 0;
    const countEnd = 2;
    const expected = helper.countFromZero;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns undefined if start value is higher than end value', () => {
    const countStart = 2;
    const countEnd = 1;
    const expected = undefined;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns undefined if start argument is undefined', () => {
    const countStart = undefined;
    const countEnd = 1;
    const expected = undefined;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns undefined if start argument is null', () => {
    const countStart = null;
    const countEnd = 1;
    const expected = undefined;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns undefined if end argument is undefined', () => {
    const countStart = 1;
    const countEnd = undefined;
    const expected = undefined;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns undefined if end argument is null', () => {
    const countStart = 1;
    const countEnd = null;
    const expected = undefined;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns undefined if start value is a string', () => {
    const countStart = 'a';
    const countEnd = 1;
    const expected = undefined;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });

  test('returns undefined if end value is a string', () => {
    const countStart = 1;
    const countEnd = 'a';
    const expected = undefined;

    const output = counter.countLikeARobot(countStart, countEnd);

    expect(output).toEqual(expected);
  });
});