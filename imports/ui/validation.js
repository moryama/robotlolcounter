// Validate start and end arguments for counter function

export const startEndValuesAreValid = (start, end) => {

  if (start === undefined || start === null) {
    return false;
  }

  if (end === undefined || end === null) {
    return false;
  }

  if (start > end) {
    return false;
  }

  if (isNaN(start) || isNaN(end)) {
    return false;
  }

  return true;
};


// Validate start and end input at UI level

export const validateUserStartEndInput = (start, end) => {

  if (isNaN(start) || isNaN(end)) {
    return 'Doesn\'t look like a number.';
  }

  if (start === '' || end === '') {
    return 'Type two numbers.';
  }

  // Parse to make sure we are comparing two numbers
  if (parseFloat(start) > parseFloat(end)) {
    return 'Number on the left should be smaller than number on the right.';
  }
};
