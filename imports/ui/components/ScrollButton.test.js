import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { ScrollButton } from './ScrollButton';


describe('<ScrollButton>', () => {

  test('clicking the button fires exactly one action', () => {

    const mockButtonAction = jest.fn();
    render(
      <ScrollButton onClick={mockButtonAction}/>
    );

    const button = screen.getByText('Top');

    fireEvent.click(button);

    expect(mockButtonAction.mock.calls).toHaveLength(1);
  });
});
