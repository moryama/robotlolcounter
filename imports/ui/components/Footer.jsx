import React from 'react';

const FooterStyle = {
  backgroundColor: '#F59F2A',
  textAlign: 'center',
  height: '5em',
  opacity: '80%',
  marginTop: '2em',
};

const ContentStyle = {
  position: 'relative',
  top: '50%',
  transform: 'translateY(-50%)', // center content vertically
  fontSize: '1.3em',
};

export const Footer = () => {
  return (
    <>
      <footer style={FooterStyle}>
        <div style={ContentStyle}>
          see code on{' '}
          <a
            href="https://gitlab.com/moryama/robotlol-counter"
            target="_blank"
            rel="noopener noreferrer"
          >
            GitLab
          </a>
          <div name="image-credits" style={{ fontSize: '0.5em' }}>
            image credits:
            <a
              href="https://commons.wikimedia.org/wiki/File:Robot-clip-art-book-covers-feJCV3-clipart.png"
              target="_blank"
              rel="noopener noreferrer"
            >
              clipartkid
            </a>
            ,
            <a
              href="https://creativecommons.org/licenses/by-sa/4.0"
              target="_blank"
              rel="noopener noreferrer"
            >
              CC BY-SA 4.0
            </a>
            , via Wikimedia Commons
          </div>
        </div>
      </footer>
    </>
  );
};
