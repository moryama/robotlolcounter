import React, { useState } from 'react';


const INITIAL_DISPLAY = 'none'; // Hide button by default
const INITIAL_BACKGROUND_COLOR = '#00b2d9';
const ONHOVER_BACKGROUND_COLOR = '#5ec7dc';
const PIXELS_FROM_TOP_AT_DISPLAY = '30';


export const ScrollButton = ({ onClick }) => {
  const [display, setDisplay] = useState(INITIAL_DISPLAY);
  const [backgroundColor, setBackgroundColor] = useState(INITIAL_BACKGROUND_COLOR);

  const ScrollButtonStyle = {
    display: display,
    position: 'sticky',
    bottom: '20px',
    left: '70%', // Place button close to list
    border: 'none',
    outline: 'none',
    backgroundColor: backgroundColor,
    color: 'white',
    padding: '15px',
    borderRadius: '7px',
    fontSize: '18px'
  };

  // Show button when user starts scrolling
  window.onscroll = () => showButtonOnScrollDown();

  const showButtonOnScrollDown = () => {
    if (document.body.scrollTop > PIXELS_FROM_TOP_AT_DISPLAY || document.documentElement.scrollTop > PIXELS_FROM_TOP_AT_DISPLAY) {
      setDisplay('block');
    } else {
      setDisplay('none');
    }
  };

  return (
    <button
      onClick={onClick}
      style={ScrollButtonStyle}
      onMouseEnter={() => setBackgroundColor(ONHOVER_BACKGROUND_COLOR)}
      onMouseLeave={() => setBackgroundColor(INITIAL_BACKGROUND_COLOR)}
    >
      Top
    </button>
  );
};
