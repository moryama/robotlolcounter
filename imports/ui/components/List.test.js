import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render, screen, within } from '@testing-library/react';
import { List } from './List';


describe('<List>', () => {

  beforeEach(() => {
    const listContent = [
      1,
      2,
      'Robot',
      4,
      'LOL'
    ];

    render(
      <List content={listContent} />
    );
  });

  test('when list has content, it renders expected number of items', () => {

    const list = screen.getByRole('list');
    const { getAllByRole } = within(list);
    const listItems = getAllByRole('listitem');

    expect(listItems.length).toBe(5);
  });
});
