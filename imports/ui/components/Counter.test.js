import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { Counter } from './Counter';


describe('<Counter>', () => {

  let component;

  beforeEach(() => {
    component = render(
      <Counter />
    );
  });

  test('is visible', () => {

    expect(component.container).toBeVisible();
  });
});
