import React, { useState } from 'react';
import { Form } from './Form';
import { List } from './List';
import { MessageBox } from './MessageBox';
import { ScrollButton } from './ScrollButton';
import { countLikeARobot } from '../counter';
import { validateUserStartEndInput } from '../validation';


const INITIAL_COUNT_START = 1;
const INITIAL_COUNT_END = 100;


export const Counter = () => {
  // User input
  const [countStart, setCountStart] = useState(INITIAL_COUNT_START );
  const [countEnd, setCountEnd] = useState(INITIAL_COUNT_END);

  const onChangeStart = ({ target }) => setCountStart(target.value);
  const onChangeEnd = ({ target }) => setCountEnd(target.value);

  // Content to render
  const listContent = countLikeARobot(countStart, countEnd);
  const message = validateUserStartEndInput(countStart, countEnd);

  // Scroll button
  const goToPageTop = () => {
    document.body.scrollTop = 0; // Safari
    document.documentElement.scrollTop = 0; // Chrome, Firefox, IE, Opera
  };

  return (
    <div style={{ fontSize: '1.3em' }}>
      <Form
        valueStart={countStart}
        valueEnd={countEnd}
        onChangeStart={onChangeStart}
        onChangeEnd={onChangeEnd}
      />
      {
        listContent
          ? <>
            <List content={listContent}/>
            <ScrollButton onClick={goToPageTop}/>
          </>
          : <MessageBox message={message}/>
      }
    </div>
  );
};
