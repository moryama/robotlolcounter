import React from 'react';
import { nanoid } from 'nanoid';
// import { ListItem } from './ListItem';


export const List = ({ content }) => {

  const ListStyle = {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'left'
  };

  return (
    <div style={ListStyle}>
      <ul>
        {content.map(item => (
          <li key={nanoid()}>{item}</li>
        ))}
      </ul>
    </div>
  );
};
