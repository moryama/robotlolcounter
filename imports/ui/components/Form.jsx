import React from 'react';


export const Form = ({ valueStart, valueEnd, onChangeStart, onChangeEnd }) => {

  const InputFieldStyle = {
    width: '11%',
    fontSize: '0.9em',
    margin: '1em'
  };

  return (
    <form>
    I can count from
      <input
        required
        id='start-input'
        value={valueStart}
        onChange={onChangeStart}
        style={InputFieldStyle}
      />
    to
      <input
        required
        id='end-input'
        value={valueEnd}
        onChange={onChangeEnd}
        style={InputFieldStyle}
      />
    </form>
  );
};
