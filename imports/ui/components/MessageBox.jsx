import React from 'react';


export const MessageBox = ({ message }) => {

  const MessageBoxStyle = {
    margin: '2em'
  };

  return (
    <div style={MessageBoxStyle}>{message}</div>
  );
};