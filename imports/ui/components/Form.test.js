import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import { Form } from './Form';


describe('<Form>', () => {

  let component;

  beforeEach(() => {
    component = render(
      <Form />
    );
  });

  test('is visible', () => {
    const form = component.container.querySelector('form');

    expect(form).toBeVisible();
  });

  test('renders two input fields', () => {
    const startInput = component.container.querySelector('#start-input');
    const endInput = component.container.querySelector('#end-input');

    expect(startInput).toBeVisible();
    expect(endInput).toBeVisible();
  });

  test('the input values can be changed', () => {
    const startInput = component.container.querySelector('#start-input');
    const endInput = component.container.querySelector('#end-input');

    fireEvent.change(startInput, {
      target: { value: 5 }
    });
    fireEvent.change(endInput, {
      target: { value: 95 }
    });

    expect(screen.getByDisplayValue(5)).toBeInTheDocument();
    expect(screen.getByDisplayValue(95)).toBeInTheDocument();
  });
});
