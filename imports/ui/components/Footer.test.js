import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { Footer } from './Footer';


describe('<Footer>', () => {

  let component;

  beforeEach(() => {
    component = render(
      <Footer />
    );
  });

  test('is visible', () => {

    expect(component.container).toBeVisible();
  });

  test('shows link to code on GitLab', () => {

    const codeLink = component.container.querySelector('a').getAttribute('href');

    expect(codeLink).toBe('https://gitlab.com/moryama/robotlol-counter');
  });

  test('shows image credits', () => {

    expect(component.container).toHaveTextContent(
      'Wikimedia Commons'
    );
  });
});
