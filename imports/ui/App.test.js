import React from 'react';
import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import { App } from './App';


describe('<App>', () => {

  let component;

  beforeEach(() => {
    component = render(
      <App />
    );
  });

  test('shows app name', () => {

    expect(component.container).toHaveTextContent(
      'RobotLOL Counter'
    );
  });

  test('shows a robot image', () => {

    const displayedImage = component.container.querySelector('img');

    expect(displayedImage.src).toContain('robot');
  });
});
