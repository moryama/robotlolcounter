// This module contains the logic for the application counter
import { startEndValuesAreValid } from './validation';

// Constants
const ROBOT_LABEL = 'Robot';
const LOL_LABEL = 'LOL';
const ROBOT_DIVISOR = 3;
const LOL_DIVISOR = 5;


const returnNumberOrWord = (i) => {

  if (i % ROBOT_DIVISOR === 0 && i % LOL_DIVISOR === 0) {
    return ROBOT_LABEL + LOL_LABEL;
  } else if (i % LOL_DIVISOR === 0) {
    return LOL_LABEL;
  } else if (i % ROBOT_DIVISOR === 0) {
    return ROBOT_LABEL;
  } else {
    return i;
  }
};

/* Return an array that contains the numbers in the given range start:end,
where numbers are replaced by words following the specifications. */

export const countLikeARobot = (startString, endString) => {

  // Convert input to a number
  const start = parseFloat(startString, endString);
  const end = parseFloat(endString);

  // Validate input
  if (!startEndValuesAreValid(start, end)) {
    return;
  }

  let arrayOfNumbersAndWords = [];

  for (let i = start; i <= end; i++) {
    const numberOrWord = returnNumberOrWord(i);
    arrayOfNumbersAndWords.push(numberOrWord);
  }

  return arrayOfNumbersAndWords;
};
