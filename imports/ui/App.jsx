import React from 'react';
import { Counter } from './components/Counter';
import { Footer } from './components/Footer';

export const App = () => {
  const ContainerStyle = {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
    /* Make the footer stick */
    minHeight: '100vh',
    flexDirection: 'column',
  };

  return (
    <>
      <div name="container" style={ContainerStyle}>
        <div>
          <h1 style={{ fontSize: '3.5em' }}>RobotLOL Counter</h1>
          <img src="/image/robot.png" alt="A friendly robot"></img>
          <Counter />
        </div>
      </div>
      <Footer />
    </>
  );
};
